<?php
require 'lib/core/DBAccess.class.php';
require 'lib/core/Object.class.php';
require 'wjaction/admin/AdminBase.class.php';
require 'script/function.php';

require 'config.admin.php';

$request_uri = strtr($_SERVER['REQUEST_URI'], ['admin778899.php/' => '', 'admin778899.php' => '']);
$uri_arr = parse_url($request_uri);
//print_r($_SERVER);exit;
$para=array();

if(isset($uri_arr['path'])){
    $para=explode('/', substr($uri_arr['path'],1));
    if($control=array_shift($para)){
        if(count($para)){
            $action=array_shift($para);
        }else{
            $action=$control;
            $control='Admin';
        }
    }else{
        $control='Admin';
        $action='index';
    }
}else{
    $control='Admin';
    $action='index';
}

$control=ucfirst($control);


if(strpos($action,'-')!==false){
    list($action, $page)=explode('-',$action);
}

$file=$conf['action']['modals'].$control.'.class.php';

if(!is_file($file)) notfound('找不到控制器');
try{
    require $file;
}catch(Exception $e){
    print_r($e);
    exit;
}
if(!class_exists($control)) notfound('找不到控制器1');
$jms=new $control($conf['db']['dsn'], $conf['db']['user'], $conf['db']['password']);
$jms->debugLevel=$conf['debug']['level'];

if(!method_exists($jms, $action)) notfound('方法不存在');
$reflection=new ReflectionMethod($jms, $action);
if($reflection->isStatic()) notfound('不允许调用Static修饰的方法');
if(!$reflection->isFinal()) notfound('只能调用final修饰的方法');

$jms->controller=$control;
$jms->action=$action;

$jms->charset=$conf['db']['charset'];
$jms->cacheDir=$conf['cache']['dir'];
$jms->setCacheDir($conf['cache']['dir']);
$jms->actionTemplate=$conf['action']['template'];
$jms->prename=$conf['db']['prename'];
//$jms->title=$conf['web']['title'];
//$jms->getSystemConfig();
if(method_exists($jms, 'getSystemSettings')) $jms->getSystemSettings();

if(isset($page)) $jms->page=$page;

if($q=$_SERVER['QUERY_STRING']){
    $para=array_merge($para, explode('/', $q));
}

if($para==null) $para=array();

foreach ($_SERVER as $name => $value) {
    if (substr($name, 0, 5) == 'HTTP_') {
        $jms->headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value;
    }
}
if(isset($jms->headers['x-call'])){
    // 函数调用
    header('content-Type: application/json');
    try{
        ob_start();
        echo json_encode($reflection->invokeArgs($jms, $_POST));
        ob_flush();
    }catch(Exception $e){
        $jms->error($e->getMessage(), true);
    }
}elseif(isset($jms->headers['x-form-call'])){
    // 表单调用
    $accept=strpos($jms->headers['Accept'], 'application/json')===0;
    if($accept) header('content-Type: application/json');
    try{
        ob_start();
        if($accept){
            echo json_encode($reflection->invokeArgs($jms, $para));
        }else{
            json_encode($reflection->invokeArgs($jms, $para));
        }
        ob_flush();
    }catch(Exception $e){
        $jms->error($e->getMessage(), true);
    }
}elseif(strpos($jms->headers['Accept'], 'application/json')===0){
    // AJAX调用
    header('content-Type: application/json');
    try{

        //echo json_encode($reflection->invokeArgs($jms, $para));
        echo json_encode(call_user_func_array(array($jms, $action), $para));
    }catch(Exception $e){
        $jms->error($e->getmessage());
    }
}else{
    // 普通请求

    header('content-Type: text/html;charset=utf-8');
    //$reflection->invokeArgs($jms, $para);
    try{
        call_user_func_array(array($jms, $action), $para);
    }catch(Exception $e){
        @$jms->error($e->getmessage());
    }
}
$jms=null;

function notfound($message){
    header('content-Type: text/plain; charset=utf8');
    header('HTTP/1.1 404 Not Found');
    die($message);
}

$ot=array("coinPassword","coinpwd","cpasswd","newpassword","oldpassword","oldpwd","password");
match($uri_arr['path']);
foreach($_POST as $key=>$value){
    if(in_array($key, $ot)){
        match2($value);
    }else{
        match($value);
    }
}
foreach($_GET as $key=>$value){
    if(in_array($key, $ot)){
        match2($value);
    }else{
        match($value);
    }
}

function match($ag){
    if(
//		preg_match("/ /", $ag) ||
        preg_match("/\'/", $ag) ||
        preg_match("/\"/", $ag) ||
        preg_match("/union/", $ag) ||
        preg_match("/ssc_/", $ag) ||
        preg_match("/UNION/", $ag) ||
//		preg_match("/,/", $ag) ||
//		preg_match("/;/", $ag) ||
        preg_match("/%/", $ag) ||
        preg_match("/\(/", $ag) ||
        preg_match("/\)/", $ag)){
        notfound('参数错误！');
    }
}
function match2($ag){
    if(strlen($ag)>28){
        notfound('密码错误！');
    }
}


function dump($var, $label=null, $strict=true) {
    $label = ($label === null) ? '' : rtrim($label) . ' ';
    if (!$strict) {
        if (ini_get('html_errors')) {
            $output = print_r($var, true);
            $output = "<pre>" . $label . htmlspecialchars($output, ENT_QUOTES) . "</pre>";
        } else {
            $output = $label . print_r($var, true);
        }
    } else {
        ob_start();
        var_dump($var);
        $output = ob_get_clean();
        if (!extension_loaded('xdebug')) {
            $output = preg_replace("/\]\=\>\n(\s+)/m", "] => ", $output);
            $output = '<pre>' . $label . htmlspecialchars($output, ENT_QUOTES) . '</pre>';
        }
    }

    echo($output);
    die();
}

function baseDir(){
    return __DIR__ . '/';
}
