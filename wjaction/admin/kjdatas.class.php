<?php
class kjdatas extends AdminBase{
	/**
	 * 开奖检测
	 */
	public $pageSize=15;
	private $encrypt_key='ssc-I/On?u8v(^PQGlCMKGLt#2R&ylR06nc315/r`k-(b94@bV<sOo#u:1@BW27,N^TQOlkRvhuu,U5mUhpfWnfZu$>-=rx)rNlSH-Oc,*oUGI*a/X`YFmgM-Bre(wfFf5Gx*qE_L@~pCtr0BOfCO#n=0w:+@l,k$y5r75Q/)~.z%&kTZDjR0OM7*xlnyyfO1JL&VtRzYrF(a~2(yz1Fz:ZB#uAR;pt`4Wg;*$+G<EWhZ~o5G$8,u=PHsGr@NI*N%sdF';	// 256位随便密码
	private $dataPort=65531;
	
	public final function tests(){
		$this->display('kjdatas/list.php');
	}
	public final function Wfcai($type){
		$this->type=$type;
		$this->display('kjdatas/wfcai.php');	
	}
	public final function ffcai($type){
		$this->type=$type;
		$this->display('kjdatas/ffcai.php');	
	}
	public final function sslcai($type){
	
		$this->type=$type;
		$this->display('kjdatas/ssl.php');	
	}
	public final function add($type, $actionNo, $actionTime){
		$para=array(
			'type'=>$type,
			'actionNo'=>$actionNo,
			'actionTime'=>$actionTime
		);
		$this->display('kjdatas/add-modal.php', 0, $para);
	}
	public final function sslkjdata($type){
		
		$this->type=$type;
		$sql="select * from {$this->prename}data_time where type={$this->type} order by actionTime";
		$times=$this->getRows($sql);
		foreach($times as $key=>$att){
			$number=1000+$att['actionNo'];
			$number=date('Ymd-', time()).substr($number,1);
			$actionNo=date('Ymd-', strtotime($att['actionTime'])).substr($att['actionNo']+1000,1);
			$data['type'] = $att['type'];
			$data['time'] = $actionNo;
			$data['number'] = $number;
			$data['data'] = $this->create_datas();
			$this->insertRow($this->prename .'datas', $data);
		}
		echo "生成成功";
		
	}
	public final function kj(){
		$para=$_GET;
		$para['key']=$this->encrypt_key;
		$url=$GLOBALS['conf']['node']['access'] . '/data/kj';
		echo $this->http_post($url, $para);
	}
	public final function added(){
		if(!$this->getValue("select data from {$this->prename}datas where type={$_POST['type']} and number='{$_POST['number']}'")){
			$para['type']=intval($_POST['type']);
			$para['number']=$_POST['number'];
			$para['data']=$_POST['data'];
			$para['time']=$this->time;
			
			
			try{
				$this->beginTransaction();
				$this->insertRow($this->prename .'datas', $para);
				//$this->addLog(17,$this->adminLogType[17].'['.$para['data'].']', 0, $this->getValue("select shortName from {$this->prename}type where id=?",$para['type']).'[期号:'.$para['number'].']');
				$this->commit();
				//throw new Exception("添加成功!");
			}catch(Exception $e){
				$this->rollBack();
				throw $e;
			}
		}else{
			
			$m = $this->getRow("select id from {$this->prename}datas where type={$_POST['type']} and number='{$_POST['number']}'");
				
			try{
				$this->beginTransaction();	
				$para['data']=$_POST['data'];
				$para['time']=$this->time;
				$para['number']=$_POST['number'];
				$para['type']=intval($_POST['type']);
						
				if($this->updateRows($this->prename .'datas', $para,'id='.$m['id'])){
				//	$this->addLog(17,$this->adminLogType[17].'['.$para['data'].']', 0, $this->getValue("select shortName from {$this->prename}type where id=?",$para['type']).'[期号:'.$para['number'].']');
					$this->commit();
					//throw new Exception("修改成功!");
				}
			}catch(Exception $e){
				$this->rollBack();
				throw $e;
			}
		}
		
	}
	public final function wfkjdata($type){
		$this->type=$type;
		$sql="select * from {$this->prename}data_time where type={$this->type} order by actionTime";
		$times=$this->getRows($sql);
		foreach($times as $key=>$att){
			$number=1000+$att['actionNo'];
			$number=date('Ymd-', time()).substr($number,1);
			$actionNo=date('Ymd-', strtotime($att['actionTime'])).substr($att['actionNo']+1000,1);
			$data['type'] = $att['type'];
			$data['time'] = $actionNo;
			$data['number'] = $number;
			$data['data'] = $this->create_data();
			$this->insertRow($this->prename .'datas', $data);
		}
		echo "生成成功";
	}
	public final function ffkjdata($type){
		$this->type=$type;
		$sql="select * from {$this->prename}data_time where type={$this->type} order by actionTime";
		$times=$this->getRows($sql);
		foreach($times as $key=>$att){
			$number=10000+$att['actionNo'];
			$number=date('Ymd-', time()).substr($number,1);
			$actionNo=date('Ymd-', strtotime($att['actionTime'])).substr($att['actionNo']+1000,1);
			$data['type'] = $att['type'];
			$data['time'] = $actionNo;
			$data['number'] = $number;
			$data['data'] = $this->create_data();
			$this->insertRow($this->prename .'datas', $data);
		}
		echo "生成成功";
	}
	public function create_data(){
			$rand='';
			for($x=0;$x<5;$x++){
				srand((double)microtime()*1000000);
				$rand.=($rand!=''?',':'').mt_rand(0,9);
			}
			return $rand;	
	}	
	public function create_datas(){
			$rand='';
			for($x=0;$x<3;$x++){
				srand((double)microtime()*1000000);
				$rand.=($rand!=''?',':'').mt_rand(0,9);
			}
			return $rand;	
	}			
}