<?php
require_once('sqlin.php');
$conf['debug']['level']=5;


$conf['db']['dsn']='mysql:host=127.0.0.1;dbname=boe2';
$conf['host']='127.0.0.1';
$conf['port']='3306';
$conf['db']['user']='root';
$conf['db']['password']='root';
$conf['db']['charset']='utf8';
$conf['db']['prename']='ssc_';

$conf['safepass']='777668';

$conf['cache']['expire']=0;
$conf['cache']['dir']='_cache/';
$conf['url_modal']=2;
$conf['action']['template']='wjinc/admin/';
$conf['action']['modals']='wjaction/admin/';
$conf['member']['sessionTime']=30*60;


error_reporting(E_ERROR & ~E_NOTICE);
ini_set('date.timezone', 'asia/shanghai');
ini_set('display_errors', 'On');