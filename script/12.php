<?php

require_once __DIR__ . '/function.php';
require_once __DIR__ . '/ParserDom.php';

action();

function action(){
    $type = 12;
    $times = getDataTime($type);
    $time = date('H:i:00', time());
    if(!isset($times[$time])) {
        logger('不是开奖时段');
        exit();
    }
    for ($i = 0; $i < 14; $i++) {
        $i > 6 && crawler($type, formatNoToNumber($times[$time]));
        sleep(5);
    }
}


function crawler($type, $number){
    if(getByNumber($type, $number)) {
        logger($number . '该次开奖已获取');
        exit();
    }
    $url = 'http://www.xjflcp.com/game/sscIndex';
    $res = curlGet($url);
    $html_dom = new ParserDom($res);
    $list = $html_dom->find('#kj_code_tab .kj_tab1',0);
    preg_match_all('/(\d{10})\s*(\d,\d,\d,\d,\d)/', $list->node->nodeValue, $res_arr);

    if(!isset($res_arr[1]) || !isset($res_arr[2]) || !$res_arr[1] || count($res_arr[1]) != count($res_arr[2])) {
        logger( '返回数据格式错误');
        exit();
    } else {
        logger( '获取到' . count($res_arr[0]) .'数据');
    }
    $res_arr = array_combine($res_arr[1], $res_arr[2]);
    $time = time();
    foreach ($res_arr as $num => $data) {
        if(10 != strlen($num)) {
            logger( '返回列表格式错误');
        }
        $period = getPeriod12($num);
        $ok = storeData($type, $period, $time, $data);
        if($ok) {
            logger($period . '开奖数据已存储');
        }
        if($number == $period) {
            //exit();
        }
    }
}

function formatNoToNumber($no){
    return date('Ymd') . sprintf('%2d', $no);
}









