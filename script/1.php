<?php

require_once __DIR__ . '/function.php';
require_once __DIR__ . '/ParserDom.php';

action();
function action(){
    $type = 1;
    $times = getDataTime($type);
    $time = date('H:i:00', time() - 60);
    if(!isset($times[$time])) {
        logger('不是开奖时段');
        exit();
    }

    for ($i = 0; $i < 10; $i++) {
        $i > 0 && crawler($type, getNumberByNo($type, $times[$time]));
        sleep(8);
    }
}
//crawler(1, 6);
function crawler($type, $number){
    if(getByNumber($type, $number)) {
        logger($number . '该次开奖已获取');
        exit();
    }
    $url = 'http://wd.apiplus.net/newly.do?token=t34f74ff53df9deeek&code=cqssc&format=json';
    $res = curlGet($url);
    $res_arr = json_decode($res, true);
    if(!count($res_arr)) {
        logger( '返回数据格式错误');
    } else {
        logger( '获取到' . count($res_arr['data']) .'数据');
    }
    $time = time();
    foreach ($res_arr['data'] as $v) {
        $period = $v['expect'];
        if(empty($period)) continue;
        $ok = storeData($type, $period, $time, $v['opencode']);
        if($ok) {
            logger($period . '开奖数据已存储');
        }
        if($number == $period) {
            //exit();
        }
    }
}

function fh($v){
    return trim($v);
}
