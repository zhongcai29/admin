<?php

require_once __DIR__ . '/function.php';
require_once __DIR__ . '/ParserDom.php';


action();
//crawler(6, 6);
function action(){
    $type = 6;
    $times = getDataTime($type);
    $time = date('H:i:00', time() - 60);
    if(!isset($times[$time])) {
        logger('不是开奖时段');
        exit();
    }

    for ($i = 0; $i < 10; $i++) {
        $i > 0 && crawler($type, getNumberByNo($type, $times[$time]));
        sleep(8);
    }
}

function crawler($type, $number){
    if(getByNumber($type, $number)) {
        logger($number . '该次开奖已获取');
        exit();
    }
    $url = 'http://wd.apiplus.net/newly.do?token=t34f74ff53df9deeek&code=gd11x5&format=json';
    $res = curlGet($url);
    $res_arr = json_decode($res, true);
    if(!count($res_arr)) {
        logger( '返回数据格式错误');
    } else {
        logger( '获取到' . count($res_arr['data']) .'数据');
    }
    $time = time();
    foreach ($res_arr['data'] as $v) {
        $period = substr($v['expect'], 2);
        if(empty($period)) continue;
        $ok = storeData($type, $period, $time, $v['opencode']);
        if($ok) {
            logger($period . '开奖数据已存储');
        }
        if($number == $period) {
            //exit();
        }
    }
}

function fh($v){
    return sprintf("%02d", trim($v));
}
