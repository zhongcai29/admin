<?php

require_once __DIR__ . '/function.php';
require_once __DIR__ . '/ParserDom.php';

action();

function action(){
    $type = 1;
    $times = getDataTime($type);
    $time = date('H:i:00', time()-120);
    if(!isset($times[$time])) {
        logger('不是开奖时段');
        exit();
    }
    for ($i = 0; $i < 20; $i++) {
        $i > 2 && crawler($type, formatNoToNumber($times[$time]));
        sleep(5);
    }
}

function crawler($type, $number){
    if(getByNumber($type, $number)) {
                logger($number . '该次开奖已获取');
                exit();
    }
    $url = 'https://m.500.com/info/kaijiang/ssc/?0_ala_h5baidu';
    $res = curlGet($url);
    $html_dom = new ParserDom($res);
    $lists = $html_dom->find('ul[class=l-flex-row]');

    $res_arr = [];
    foreach ($lists as $list) {
        $text = $list->getPlainText();
        if(preg_match('/^(\d{11}).*?(\d,\d,\d,\d,\d)/s', $text, $preg_arr)) {
            $res_arr[$preg_arr[1]] = $preg_arr[2];
        }
    }
    logger( '获取到' . count($res_arr) .'数据');
    $time = time();
    foreach ($res_arr as $period => $v) {
        if(11 != strlen($period)) {
            logger( '返回列表格式错误');
        }
        $ok = storeData($type, $period, $time, $v);
        if($ok) {
            logger($period . '开奖数据已存储');
        }
        if($number == $period) {
            //exit();
        }
    }
}
function formatNoToNumber($no){
    return date('Ymd') . sprintf('%03d', $no);
}









