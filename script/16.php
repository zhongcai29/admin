<?php

require_once __DIR__ . '/function.php';
require_once __DIR__ . '/ParserDom.php';

action();

function action(){
    $type = 16;
    $times = getDataTime($type);
    $time = date('H:i:00', time() - 60);
    if(!isset($times[$time])) {
        logger('不是开奖时段');
        exit();
    }

	for ($i = 0; $i < 6; $i++) {
		crawler($type, formatNoToNumber($times[$time]));
        sleep(5);
    }
}


function crawler($type, $number){

    if(getByNumber($type, $number)) {
        logger($number . '该次开奖已获取');
        exit();
    }
    $url = 'http://jx11x5.icaile.com/';
    $res = curlGet($url);
    $html_dom = new ParserDom($res);
    $list = $html_dom->find('table#fixedtable',0);
    preg_match_all('/\d{8}\s*\*\*\s*[a-zA-Z]{4}=M\s*\*\*\s*[a-zA-Z]{4}=M\s*\*\*\s*[a-zA-Z]{4}=M\s*\*\*\s*[a-zA-Z]{4}=M\s*\*\*\s*[a-zA-Z]{4}=M/', $list->node->nodeValue, $res_arr);
    if(!isset($res_arr[0]) || !$res_arr[0]) {
        logger( '返回数据格式错误');
    } else {
        logger( '获取到' . count($res_arr[0]) .'数据');
    }
    $time = time();
    foreach ($res_arr[0] as $v) {
    	$v = trans($v);
        if(6 != count(explode(',', $v))) {
            logger( '返回列表格式错误');continue;
        }
        $period = substr($v, 0, 8);
        $ok = storeData($type, $period, $time, substr($v, 9));
        if($ok) {
            logger($period . '开奖数据已存储');
        }
        if($number == $period) {
            //exit();
        }
    }
}

function formatNoToNumber($no){
    return 	date('ymd').substr(100+$no,1);

}













