<?php

require_once __DIR__ . '/function.php';
require_once __DIR__ . '/ParserDom.php';

action();
function action(){
    $type = 7;
    $times = getDataTime($type);
    $time = date('H:i:00', time() - 60);
    if(!isset($times[$time])) {
        logger('不是开奖时段');
        exit();
    }

    for ($i = 0; $i < 7; $i++) {
        crawler($type, getNumberByNo($type, $times[$time]));
        sleep(8);
    }
}


function crawler($type, $number){
    if(getByNumber($type, $number)) {
        logger($number . '该次开奖已获取');
        exit();
    }
    $url = 'http://hao123.lecai.com/lottery/ajax_lottery_draw_phaselist.php?lottery_type=20';
    $file = __DIR__ . "/data/$type";
    exec('curl '.$url. " > $file");
    $res = is_file($file)?file_get_contents($file):'';
    $res_arr = json_decode($res, 1);
    if(!isset($res_arr['data']) || !$res_arr['data']) {
        logger( '返回数据格式错误');
    } else {
        logger( '获取到' . count($res_arr['data']['data']) .'数据');
    }
    $time = time();
    foreach ($res_arr['data']['data'] as $v) {
        $period = $v['phase'];
        $ok = storeData($type, $v['phase'], $time, implode(',', $v['result']['result'][0]['data']));
        if($ok) {
            logger($period . '开奖数据已存储');
        }
        if($number == $period) {
            //exit();
        }
    }
}














