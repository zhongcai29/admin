<?php

require_once __DIR__ . '/function.php';

if(isset($argv[1]) && is_numeric($argv[1])) {
	kj($argv[1]);
}

function kj($id){
	$bet_info = getBetById($id);
	$type_info = getTypeInfo($bet_info['type']);
	if($type_info['is_hand']) {
		return;
	}
	$win_code = getByNumber($bet_info['type'], $bet_info['actionNo']);
	if(empty($win_code)) {
		return;
	}
	db()->beginTransaction();
	db()->update("UPDATE ssc_bets SET status = status-1 WHERE id={$id}");
	$bet_info = getBetById($id);
	if($bet_info['status'] == -1){
		db()->commit();
	}else{
		db()->rollBack();
		return;
	}
	$func = getPlayFunc($bet_info['type'], $bet_info['playedId']);
	if(is_null($func)) {
		updateBet($bet_info, 0, $win_code, 2);
		return;
	}
	handleBet($bet_info, $win_code, $func);
}